import datetime

from .features.examples.greeting_provider import GreetingProviderBase


class GreetingProvider(GreetingProviderBase):
    """
    Example implementation of a minimum Feature. Provides a Greeting to the Client
    and a StartYear property, informing about the year the Server has been started.
    """

    def __init__(self):
        super().__init__()
        self.__start_year = datetime.datetime.now().year

    async def say_hello(self, name: str) -> str:
        return f"Hello {name}, thanks for connecting!"

    async def start_year(self):
        return self.__start_year
