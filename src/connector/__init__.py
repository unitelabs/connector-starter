from unitelabs.cdk import Connector
from unitelabs.cdk.features.test.basic_data_types_test import BasicDataTypesTest
from unitelabs.cdk.features.test.observable_command_test import ObservableCommandTest
from unitelabs.cdk.features.test.observable_property_test import ObservablePropertyTest
from unitelabs.cdk.features.test.unobservable_command_test import UnobservableCommandTest
from unitelabs.cdk.features.test.unobservable_property_test import UnobservablePropertyTest

from .greeting_provider import GreetingProvider


async def create_app():
    """Creates the connector application"""
    app = Connector(
        {
            "sila_server": {
                "name": "UniteLabs Example",
                "type": "Example",
                "description": "A UniteLabs SiLA Python Example Server",
                "version": "0.1.0",
                "vendor_url": "https://unitelabs.io/",
            }
        }
    )

    app.register(BasicDataTypesTest())
    app.register(ObservableCommandTest())
    app.register(ObservablePropertyTest())
    app.register(UnobservableCommandTest())
    app.register(UnobservablePropertyTest())

    app.register(GreetingProvider())

    return app
